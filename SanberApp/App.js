// import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
// import YoutubeUI from './tugas/tugas12/App'
import LoginPage from './tugas/tugas13/App'
import TodoApp from './tugas/tugas14/App'
import Tugas15 from './tugas/tugas15/index'
import TugasNavigation from './tugas/tugasNavigation/index'
import Quiz3 from './tugas/quiz3/index'

export default function App() {
  return (
    // <YoutubeUI />
    // <LoginPage />
    // <TodoApp />    
    // <Tugas15 />
    // <TugasNavigation />
    <Quiz3 />

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '10px'
  },
});
