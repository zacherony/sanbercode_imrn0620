import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
// import  AboutScreen from './pages/AboutScreen'
import LoginScreen from './pages/LoginScreen';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
          <LoginScreen />
          {/* <AboutScreen /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    elevation: 3,
    marginTop: 15,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  rightNav: {
    flexDirection: 'row'
  },

  navItem: {
    marginLeft: 25
  },

  body: {
    flex: 1
  },

  tabBar: {
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },

  tabItem: {
    alignItems: 'center',
    justifyContent: 'center'
  },

  tabTitle: {
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 4
  }
});