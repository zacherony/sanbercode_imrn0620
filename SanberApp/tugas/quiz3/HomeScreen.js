import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';

import data from './data.json';

const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
      totalPrice: 0,
    }
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  updatePrice(price) {
    //? #Soal Bonus (10 poin) 
    //? Buatlah teks 'Total Harga' yang akan bertambah setiap kali salah satu barang/item di klik/tekan.
    //? Di sini, buat fungsi untuk menambahkan nilai dari state.totalPrice dan ditampilkan pada 'Total Harga'.
    
    // Kode di sini
    // console.log(this.state.totalPrice += price )

    let addPrice = parseInt(price);
    let total = this.state.totalPrice + addPrice;
    this.setState({totalPrice: total})

      // this.state.totalPrice += Number(price)
    // this.state.totalPrice += price
    // return this.currencyFormat( Number(this.state.totalPrice) )
  }

  render() {
    console.log(data)
    return (
      <View style={styles.container}>
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hai,{'\n'}
            {/* tempat username */}
              <Text style={styles.headerText}>{ this.props.route.params.userName }</Text>
            </Text>

            {/* //? #Soal Bonus, simpan Total Harga dan state.totalPrice di komponen Text di bawah ini */}
            <Text style={{ textAlign: 'right' }}>Total Harga{'\n'}
              <Text style={styles.headerText}>{ this.currencyFormat(Number(this.state.totalPrice)) }</Text>
            </Text>
          </View>
          <View>

          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder='Cari barang..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>

        {/* 
        //? #Soal No 2 (15 poin)
        //? Buatlah 1 komponen FlatList dengan input berasal dari data.json
        //? dan pada prop renderItem menggunakan komponen ListItem -- ada di bawah --
        //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal)

        // Lanjutkan di bawah ini!
        */}

          <FlatList
            data={data.produk}
            numColumns={2}
            renderItem={ (item) => 
              <ListItem 
              data={item.item} 
              onPress={ () => {
                this.updatePrice(item.item.harga)
              }}
              /> }
            keyExtractor={(data) => data.id.toString()}
          />

      </View>
    )
  }
};

class ListItem extends React.Component {
  constructor(props) {
      super(props)
      this.state = {

      }
  }

  componentDidMount(){
    console.log(this.props);
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };
  //? #Soal No 3 (15 poin)
  //? Buatlah styling komponen ListItem, agar dapat tampil dengan baik di device

  render() {
    const data = this.props.data
    let home = new HomeScreen()
    return (
      <View style={styles.wrapperItem}>
        <View style={styles.itemContainer}>
          <Image source={{ uri: data.gambaruri }} style={styles.itemImage} resizeMode='contain' />
          <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.nama}</Text>
          <Text style={styles.itemPrice}>{this.currencyFormat(Number(data.harga))}</Text>
          <Text style={styles.itemStock}>Sisa stok: {data.stock}</Text>
          <Button 
            style={styles.itemButton} 
            title='BELI' color='blue' 
            onPress={ this.props.updatePrice } 
          />
        </View>       
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },

  wrapperItem: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems:'stretch',
    backgroundColor: '#dddddd',    
  },
  //? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.44,
    flexDirection: 'column',
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    padding:5,
    marginVertical: 10    
  },
  itemImage: {
    width: 100,
    height: 100,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  itemName: {
    fontWeight: 'bold',
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 20
  },
  itemPrice: {
  },
  itemStock: {
  },
  itemButton: {
    flex: 1
  },
  buttonText: {
  }
})
