import React from 'react';
import { View, Text, Image, ScrollView, TextInput, FlatList, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import BodySkill from './bodySkill';
import data from '../skillData.json';

export default class SkillScreen extends React.Component {
    render() {        
        return (
            <View style={styles.container}>
                <View style={styles.header}>

                    <View style={styles.logoWrapper}>
                        <Image source={require('../images/logo.png')} style={styles.logo} />
                    </View>

                        <View style={styles.logoUser}>

                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Icon name="account-circle" size={40} style={{color: '#3EC6FF'}} />
                                <View style={styles.logoUser}>
                                    <Text style={{fontSize:14, padding: 3 }} >Hi,</Text>
                                    <Text style={{fontSize:20, padding: 3,  color: '#003366' }} >Zacherony</Text>
                                </View>
                            </View>

                        </View>
                    
                    <View>
                        <Text style={{fontSize:40, padding: 3, color: '#003366' }} >SKILL</Text>
                    </View>

                    <View
                        style={{
                            borderBottomColor: '#3EC6FF',
                            borderBottomWidth: 3,
                            paddingVertical: 5,
                            marginBottom: 10,
                            width: '100%'

                        }}
                    />

                </View>

                <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                    <TouchableOpacity
                        style={styles.button}
                    >
                        <Text style={{fontSize:12}}>Library / Framework</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.button}
                    >
                        <Text style={{fontSize:12}}>Bahasa Pemrograman</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.button}
                    >
                        <Text style={{fontSize:12}}>Teknologi</Text>
                    </TouchableOpacity>
                </View>
                
                <View style={styles.body}>

                    <ScrollView style={styles.container} >
                        <BodySkill data={data} />
                    </ScrollView>
                </View>


            </View>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  
  header: {

  },
  body:{
    flex: 1
  },
  logoUser: {
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  
  logoWrapper: {
    alignSelf: 'flex-end'
  },
  
  logo: {    
    width: 250,
    height: 70
  },
  button: {
    alignItems: "center",
    backgroundColor: "#B4E9FF",
    borderRadius: 4,
    paddingHorizontal : 7,
    paddingVertical: 10
  },
  separator: {
    height: 1,
    backgroundColor: '#707080',
    width: '100%',      
  },





})