import { UBAH_DATA_PROVINSI } from "../constants/actionTypes"

const initialState = {
  provinsi: []
}


const provinsiReducer = ( state = initialState, action ) => {
    switch(action.type)
    {
        case 'UBAH_DATA_PROVINSI':
            return { 
                provinsi: ''
            }

        default:
            return state
    }

  return state
}

export default provinsiReducer