import { INCREASE_COUNTER, DECREASE_COUNTER } from "../constants/actionTypes"

const initialState = {
  uname: '',
  pwd: '',
  isError: false
}


const loginReducer = ( state = initialState, action ) => {
    switch(action.type)
    {
        case 'USERNAME_CHANGE':
            return { 
              ...state,
              uname: state.uname }
            // return Object.assign({}, state, { uname: action.text })

        case 'PASSWORD_CHANGE':
            return { 
              ...state,
              pwd: state.pwd }

        default:
            return state
    }
}

export default loginReducer