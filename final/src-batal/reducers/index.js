import { combineReducers } from 'redux'
import CounterReducer from './counterReducer'

const reducer = combineReducers({
	counter: CounterReducer
})
 
export default reducer