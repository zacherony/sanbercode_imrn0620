import React, {Component} from 'react';
import { StyleSheet, Text, View, FlatList} from 'react-native';
import {connect} from 'react-redux'
// import {ListItem } from 'react-native-elements'
import Axios from 'axios'

class ProvList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {},
            isLoading: true,
            isError: false,
          };
      
    }

	componentDidMount() {
	    Axios.get( state.prefik_url+ 'indonesia/provinsi' )
	    .then( res => {
	        const provinsi = res.data
	        console.log(provinsi)
	        this.setState({ provinsi })
        })

        getProvinsi = async () => {
            try {
            const response = await Axios.get(
                'https://api.kawalcorona.com/indonesia/provinsi'
            );
            this.setState({ isError: false, isLoading: false, data: response.data });
            } catch (error) {
            this.setState({ isLoading: false, isError: true });
            }
        };
        

	}

    render() {
        return (
            <View>
                <View>
                    
                </View>
                <View>                
                      <FlatList
                        data={ this.state.provinsi }
                        numColumns={2}
                        renderItem={ (item) => 
                          <ListItem 
                          data={item.item} 
                          onPress={ () => {
                            this.updatePrice(item.item.harga)
                          }}
                          /> }
                        keyExtractor={(data) => data.id.toString()}
                      />
                </View>
            </View>
        )
    }

    tampilkanData() {

    }

//end of HomeScreen
}

function mapStateToProps(state) {
    return {
            data: {},
            isLoading: true,
            isError: false,
    }
}

function mapDispatchToProps( dispatch ) {
    return {
        increaseCounter: () => dispatch({ type: 'INCREASE_COUNTER' }),
        decreaseCounter: () => dispatch({ type: 'DECREASE_COUNTER' }),
    }
}

export default connect( mapStateToProps, mapDispatchToProps )(ProvList)