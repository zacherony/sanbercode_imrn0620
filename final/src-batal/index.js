import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import "react-native-gesture-handler";

import { Provider } from 'react-redux'
import store from './store'

import LoginScreen from "./pages/login";
import HomeScreen from "./pages/home";
import DetailScreen from "./pages/detail";
import AboutScreen from "./pages/about";

const Stack = createStackNavigator()

export default class App extends React.Component {
  render() {
    return (
        <Provider store={store} >
            <NavigationContainer>
                <Stack.Navigator initialRouteName="Login">
                <Stack.Screen
                    name="Login"
                    component={LoginScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Home"
                    component={HomeScreen}
                    options={{ headerTitle: "List Propinsi" }}
                />
                <Stack.Screen
                    name="Detail"
                    component={DetailScreen}
                    options={{ headerTitle: "Detail Propinsi" }}
                />
                <Stack.Screen
                    name="about"
                    component={AboutScreen}
                    options={{ headerTitle: "Tentang Aplikasi" }}
                />                  
                </Stack.Navigator>
            </NavigationContainer>
      </Provider>
    )
  }
}
