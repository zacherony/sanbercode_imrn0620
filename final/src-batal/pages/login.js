import React from "react";
import { View, Text, Image, TextInput, StyleSheet, TouchableOpacity } from "react-native";
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";

import {connect} from 'react-redux'
import { registerRootComponent } from "expo";

class LoginScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: "",
            password: "",
            isError: false
        }
    }
    

    render() {
        console.log(this.state.username);
        return (

            <View style={styles.container}>
                <View style={styles.headless}>
                    <View>
                        <Text style={{fontSize: 50 }}>corona
                            <Text style={{color: 'red'}}>Virus</Text>
                        </Text>
                        <Text style={{textAlign: 'right',color: 'white', fontSize: 25}}>Locator</Text>
                    </View>
                    <Image source={require('../images/logo.png')} style={styles.logo} />
                </View>
                <View style={{padding: 10}}>
                    <View style={styles.inputWrapper} >
                    <Text style={styles.inputLabel}>Username</Text>
                    <TextInput 
                        value ={this.state.username}
                        label="Username"
                        style={styles.inputs}
                        onChangeText={ username => this.props.unameChanged }
                        // onChangeText={text => onChangeText(text)}
                    />
                    {/* <Text>{this.state.username}</Text> */}
                    </View>
                    <View style={styles.inputWrapper} >
                        <Text style={styles.inputLabel}>Password</Text>
                        <TextInput
                            label="Password"
                            style={styles.inputs}
                            secureTextEntry={true}
                            value ={this.state.password}
                            onChangeText={ password => this.props.password }
                        />
                        <TouchableOpacity
                            onPress={() => registerUser() }
                        >
                            <Text style={{marginTop: 5,textAlign:'right'}}>belum registrasi >> </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{padding: 10}}>
                    <TouchableOpacity
                        style={styles.button1}
                        onPress={() => this.props.navigation.navigate("Home")}
                    >
                        <Text style={{fontSize: 18, color: 'white'}}>     Masuk     </Text>
                    </TouchableOpacity>
                </View>
            </View>
            )
    }

}

function registerUser() {
    alert('sayang sekali, anda belum registrasi...')
}

const mapStateToProps = (state) => {
    return {
        username: state.uname,
        password: state.pwd,
        isError: state.isError
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        // unameChanged: (evt) => {
        //     console.log('yes', evt );
            
        //     // const action = { type: 'USERNAME_CHANGE', text: evt.target.value }
        //     // dispatch( action )
        // },
        unameChanged: () => dispatch({ type: 'USERNAME_CHANGE', username: }),
        pwdChanged: () => dispatch({ type: 'PASSWORD_CHANGE' }),
    }
}

export default connect( mapStateToProps, mapDispatchToProps )(LoginScreen)


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#cccccc',
        paddingBottom: 20
        // padding: 20        
    },
    headless: {
        justifyContent: 'center', 
        alignItems:'center', 
        height:'65%', 
        width:'100%', 
        backgroundColor: '#003366',
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25,
    },
    logo: {
        width: 300,
        height: 100
    },
    loginText: {
        justifyContent: 'center',
        alignItems:'center',
        fontSize: 25,
        marginTop: 50,
        color: '#3c3c3c'
    },
    inputLabel: {
        fontSize: 15,
        marginBottom:10
    },
    inputWrapper: {
        padding: 5,
        flexDirection: 'column',
        alignItems: 'stretch',
        marginTop: 5,
        marginBottom: 5,        
    },
    inputs: {
        height: 35,
        padding: 5,
        borderWidth: 2, 
        borderColor: '#f2f2f2',
        borderBottomColor: 'grey',
        backgroundColor: '#f2f2f2',
        textTransform: "uppercase"
    },
    button1: {
      alignItems: "center",
      backgroundColor: "#003366",
      borderRadius: 15,
      padding: 10
    },
    button2: {
      alignItems: "center",
      backgroundColor: "#003366",
      borderRadius: 15,
      padding: 10
    }

})