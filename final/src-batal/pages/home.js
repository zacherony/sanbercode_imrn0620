import React from "react";
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from "react-native";
import ProvList from '../components/provlist'
import {connect} from 'react-redux'

const DEVICE = Dimensions.get("window");

class HomeScreen extends React.Component {

    render() {
        return (
            <View>
                <View>
                    
                </View>
                <View>
                    <ProvList />
                </View>
            </View>
        )
    }

    tampilkanData() {

    }

//end of HomeScreen
}  

function mapStateToProps(state) {
    return {
        userName: state.userName,
        password: state.password,
        isError: state.isError,
        counter: state.counter        
    }
}

function mapDispatchToProps( dispatch ) {
    return {
        increaseCounter: () => dispatch({ type: 'INCREASE_COUNTER' }),
        decreaseCounter: () => dispatch({ type: 'DECREASE_COUNTER' }),
    }
}

export default connect( mapStateToProps, mapDispatchToProps )(HomeScreen)