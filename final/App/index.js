import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import "react-native-gesture-handler";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// import { Provider } from 'react-redux'
// import store from './store'

import LoginScreen from "./pages/login";
import HomeScreen from "./pages/home";
import DetailScreen from "./pages/detail";
import AboutScreen from "./pages/about";

const RootStack = createStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator>
    {/* Halaman Login screen ------------------------------------------- */}
    <RootStack.Screen 
    name="Login"
    options={{ headerShown: false }}
    component={LoginScreen} />

  {/* Halaman Utama menampilkan daftar provinsi di Indonesia ----------- */}
    <RootStack.Screen
      name="Utama"
      component={HomeScreen}
      options={({ navigation }) => ({
        headerStyle: {
            backgroundColor: '#003366',
            borderBottomColor: 'transparent'
        },
        headerTintColor: '#fff',
        headerLeft: () => (
            <Icon
              name="arrow-left"
              size={30}
              color="white"
              style={{ marginLeft: 20,  }}
              onPress={() => navigation.navigate("Login")}
            />
          ),
      })}
    />

  {/* Halaman Detail menampilkan detail provinsi ---------------------- */}
    <RootStack.Screen
      name="detail"
      component={DetailScreen}
      options={({ navigation }) => ({
        headerStyle: {
            backgroundColor: '#003366',
            borderBottomColor: 'transparent'
        },
        headerTintColor: '#fff',
        headerLeft: () => (
            <Icon
              name="arrow-left"
              size={30}
              color="white"
              style={{ marginLeft: 20,  }}
              onPress={() => navigation.navigate("Utama")}
            />
          ),
      })}
    />

  {/* Halaman About menampilkan informasi tentang App ini -------------- */}
    <RootStack.Screen 
       name="About"
       component={AboutScreen} 
    />

  </RootStack.Navigator>
)

export default class App extends React.Component {
        
  render() {
    return (
        <NavigationContainer>
            <RootStackScreen />
        </NavigationContainer>
    



    )
  }
}
