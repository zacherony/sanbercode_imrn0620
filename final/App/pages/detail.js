import React from "react";
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from "react-native";
import Tabu from '../components/tabu'

export default class DetailScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {},
            isLoading: true,
            isError: false,
          };      
    }

    render() {
        console.log(JSON.stringify(this.props.route.params.dataDetail));
        const datapropinsi = this.props.route.params.dataDetail
        return (
            <View style={styles.container}>
                <View style={styles.headr}>
                    <Text style={styles.nmprop}>{datapropinsi[0].attributes.Provinsi }</Text>
                </View>
                <View style={{ minHeight: '84%', maxHeight: '84%', flexDirection:'row', justifyContent: "center", alignItems:"flex-start", marginTop:20 }}>
                    <View style={styles.positif}>
                        <Text style={{fontSize: 16,fontWeight:"bold"}}>Positif</Text>
                        <Text style={{fontSize: 20, fontWeight:"bold"}}>{ datapropinsi[0].attributes.Kasus_Posi }</Text>
                    </View>
                    <View style={styles.sembuh}>
                        <Text style={{fontSize: 16,fontWeight:"bold"}}>Sembuh</Text>
                        <Text style={{fontSize: 20, fontWeight:"bold"}}>{ datapropinsi[0].attributes.Kasus_Semb }</Text>
                    </View>
                    <View style={styles.meninggal}>
                        <Text style={{fontSize: 16,fontWeight:"bold"}}>Meninggal</Text>
                        <Text style={{fontSize: 20, fontWeight:"bold"}}>{ datapropinsi[0].attributes.Kasus_Meni }</Text>
                    </View>
                </View>

                <Tabu navigation={this.props.navigation}/>

            </View>
        )
    }



//end of HomeScreen
}  

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: '#cccccc',
        paddingBottom: 20
        // padding: 20        
    },
    nmprop: {
        fontSize: 25,
        textTransform: 'uppercase'
    },
    headr: {
        padding: 10,
        justifyContent: "center",
        alignItems:"center",
        backgroundColor: 'orange',
        width: '100%'
    },
    positif: {
        padding: 10,
        height: 170,
        width: 100,
        backgroundColor: 'yellow',
        margin: 10,
        justifyContent: "space-around",
        alignItems: "center"
    },
    sembuh: {
        padding: 10,
        height: 170,
        width: 100,
        backgroundColor: 'green',
        margin: 10,
        justifyContent: "space-around",
        alignItems: "center"
    },
    meninggal: {
        padding: 10,
        height: 170,
        width: 100,
        backgroundColor: 'red',
        margin: 10,
        justifyContent: "space-around",
        alignItems: "center"
    }    

})