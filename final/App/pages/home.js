import React from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import ProvList from '../components/provlist'
import Tabu from '../components/tabu'

const DEVICE = Dimensions.get("window");

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          searchText: '',
          totalPrice: 0,
        }
      }

    render() {
        return (
            <View>
                <View style={{ minHeight: '90%', maxHeight: '90%'}}>
                    <ProvList navigation={ this.props.navigation } />
                </View>
                <Tabu navigation={this.props.navigation}/>
            </View>
        )
    }

//end of HomeScreen
}  

const styles = StyleSheet.create({
    container: {
      flex: 1,
    }
})