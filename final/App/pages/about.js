import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Tabu from '../components/tabu'

export default class AboutScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
          searchText: '',
          totalPrice: 0,
        }
      }    
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headless}>
                    <View>
                        <Text style={{fontSize: 50 }}>corona
                            <Text style={{color: 'red'}}>Virus</Text>
                        </Text>
                        <Text style={styles.judul2}>Locator</Text>
                    </View>
                    <Image source={require('../images/logo.png')} style={styles.logo} />
                </View>

                <View>
                    <Text style={styles.judulPage} >About This App</Text>
                </View>
                <View style={{alignItems: 'center', marginTop: 10, padding: 10 }}>
                    <Text style={styles.perkataan}>
                        Aplikasi ini dibuat untuk membantu para petugas untuk memantau perkembangan 
                        dan penyebaran corona Virus di Indonesia.
                    </Text>
                    <Text style={styles.perkataan}>
                        Author mempersilahkan, semua orang untuk ikut berpartisipasi, mengembangkan
                        aplikasi ini untuk kepentingan khalayak ramai
                    </Text>                    
                </View>
                <View style={{height:'25%'}}></View>
                <View style={{flexDirection:'row', justifyContent: 'center', alignItems:'flex-end'}} >
                        <View style={styles.itemSocial}>
                            <Icon name="instagram" size={25} />
                            <Text style={{fontSize:12, marginTop:20}} >@zach</Text>
                        </View>
                        <View style={styles.itemSocial}>
                            <Icon name="facebook" size={25} />
                            <Text style={{fontSize:12, marginTop:20}} >@zach</Text>
                        </View>
                        <View style={styles.itemSocial}>
                            <Icon name="twitter" size={25} />
                            <Text style={{fontSize:12, marginTop:20}} >@zach</Text>
                        </View>                    
                    </View>
                    <Tabu navigation={this.props.navigation} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',        
    },
    judul2: {
        textAlign: 'right',color: 'white', fontSize: 20
      },
    headless: {
        justifyContent: 'center', 
        alignItems:'center', 
        height:'30%', 
        width:'100%', 
        backgroundColor: '#003366',
    },
    logo: {
        width: 200,
        height: 70
    },    
    judulPage: {
        fontSize: 25
    },
    perkataan: {
        fontSize: 15,
        alignItems: 'stretch'
    },
    pp: {
        marginTop: 10,
        color : '#dddddd',
        fontSize: 150,
    },
    ctn: {
        flexDirection: 'column',
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: '#EFEFEF',
        marginVertical: 5,
        padding: 10,
        borderRadius: 5
    },
    itemSocial : {
        flexDirection: 'column', alignItems: 'center', paddingHorizontal: 15
    }
})