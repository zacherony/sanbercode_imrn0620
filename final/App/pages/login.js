import React from "react";
import { View, Text, Image, TextInput, StyleSheet, TouchableOpacity } from "react-native";
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";

// import {connect} from 'react-redux'
// import { registerRootComponent } from "expo";

export default class LoginScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          username: '',
          password: '',
          isError: false,
        }
      }

    loginHandler() {
        // console.log(this.state.username, ' ', this.state.password)
        if (this.state.password=='' || this.state.password !=='') {
          this.state.isError = false
          this.props.navigation.navigate("Utama", { 
            username: this.state.username
          })
    
        //   console.log(this.state.username)
        } else {
          this.state.isError = true
        }
    }    

    render() {
        console.log(this.state.username);
        return (

            <View style={styles.container}>
                <View style={styles.headless}>
                    <View>
                        <Text style={{fontSize: 50 }}>corona
                            <Text style={{color: 'red'}}>Virus</Text>
                        </Text>
                        <Text style={styles.judul2}>Locator</Text>
                    </View>
                    <Image source={require('../images/logo.png')} style={styles.logo} />
                </View>
                <View style={{padding: 10}}>
                    <View style={styles.inputWrapper} >
                    <Text style={styles.inputLabel}>Username</Text>
                    <TextInput
                        style={styles.inputs}
                        value={this.props.username}
                        onChangeText={username => this.setState({ username })}
                    />                    
                    {/* <TextInput 
                        value ={this.state.username}
                        label="Username"
                        style={styles.inputs}
                        onChangeText={userName => this.setState({ username })}
                        // onChangeText={text => onChangeText(text)}
                    /> */}

                    </View>
                    <View style={styles.inputWrapper} >
                        <Text style={styles.inputLabel}>Password</Text>
                        <TextInput
                            style={styles.inputs}
                            onChangeText={password => this.setState({ password })}
                            secureTextEntry={true}
                        />                        
                        {/* <TextInput
                            label="Password"
                            style={styles.inputs}
                            secureTextEntry={true}
                            value ={this.state.password}
                            onChangeText={ password => this.props.password }
                        /> */}
                        <TouchableOpacity
                            onPress={() => registerUser() }
                        >
                            <Text style={{marginTop: 5,textAlign:'right'}}>belum registrasi >> </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{padding: 10}}>
                 {/*<Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>*/}
                    <TouchableOpacity
                        style={styles.button1}
                        onPress={() => this.loginHandler()}
                    >
                        <Text style={{fontSize: 18, color: 'white'}}>     Masuk     </Text>
                    </TouchableOpacity>
                </View>
            </View>
            )
    }

}

function registerUser() {
    alert('sayang sekali, anda belum registrasi...')
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: '#cccccc',
        paddingBottom: 20
        // padding: 20        
    },
    headless: {
        justifyContent: 'center', 
        alignItems:'center', 
        height:'45%', 
        width:'100%', 
        backgroundColor: '#003366',
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25,
    },
    judul2: {textAlign: 'right',color: 'white', fontSize: 25},
    logo: {
        width: 300,
        height: 100
    },
    loginText: {
        justifyContent: 'center',
        alignItems:'center',
        fontSize: 25,
        marginTop: 50,
        color: '#3c3c3c'
    },
    inputLabel: {
        fontSize: 15,
        marginBottom:10
    },
    inputWrapper: {
        padding: 5,
        flexDirection: 'column',
        alignItems: 'stretch',
        marginTop: 5,
        marginBottom: 5,        
    },
    inputs: {
        height: 35,
        padding: 5,
        borderWidth: 2, 
        borderColor: '#f2f2f2',
        borderBottomColor: 'grey',
        backgroundColor: '#f2f2f2',
        textTransform: "uppercase"
    },
    button1: {
      alignItems: "center",
      backgroundColor: "#003366",
      borderRadius: 15,
      padding: 10
    },
    button2: {
      alignItems: "center",
      backgroundColor: "#003366",
      borderRadius: 15,
      padding: 10
    },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },    

})