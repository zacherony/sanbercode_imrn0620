import React from 'react';
import { StyleSheet, Text, View, FlatList, Dimensions, ActivityIndicator, TouchableOpacity} from 'react-native';
import { MaterialCommunityIcons } from "@expo/vector-icons";
// import { withNavigation } from '@react-navigation/native';

// import {connect} from 'react-redux'
// import {ListItem } from 'react-native-elements'
import Axios from 'axios'

const DEVICE = Dimensions.get('window')

export default class ProvList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {},
            isLoading: true,
            isError: false,
            dataDetail: {}
          };      
    }

    componentDidMount() {
          this.getProvinsi()
    }

    getProvinsi = async () => {
        try {
          const response = await Axios.get(
              `https://api.kawalcorona.com/indonesia/provinsi`
          );
          this.setState({ isError: false, isLoading: false, data: response.data });
          // console.log(JSON.stringify( this.state.data ) );
                  
        } catch (error) {
          this.setState({ isLoading: false, isError: true });
        }
    };

    getDetail = (id) => {
      console.log(this.props.navigation);
      // alert(id)      
        let dataProv = this.state.data
        const selectedProvince = dataProv.filter( prov => prov.attributes.FID === id );
        this.setState({ dataDetail: selectedProvince });
        return selectedProvince
        // console.log(selectedProvince);
        
        // this.props.navigation.push("detail")
        //   dataDetail: this.state.dataDetail
        // })
    }

    render() {
      // const { navigation } = this.props
      // console.log(this.props.navigation);
      //  If load data
      if (this.state.isLoading) {
        return (
          <View style={{ alignItems: "center", justifyContent: "center", flex: 1 }} >
            <ActivityIndicator size="large" color="red" />
          </View>
        );
      }

      // If data not fetch
      else if (this.state.isError) {
        return (
          <View style={{ alignItems: "center", justifyContent: "center", flex: 1 }} >
            <Text>Terjadi Error Saat Memuat Data</Text>
          </View>
        );
      }
        return (
            <View>
                <View style={{height:50, justifyContent:'center', alignItems:'center'}}>
                          <Text style={{fontSize:16}}>Daftar Provinsi di Indonesia</Text>
                </View>
                <FlatList
                    data={this.state.data}
                    renderItem={({ item }) => (
                      <View>
                        <TouchableOpacity 
                        style={styles.button}
                        onPress={ () => {
                          this.props.navigation.navigate('detail',{
                            dataDetail : this.getDetail(item.attributes.FID)
                          })
                        }}
                        >
                            <Text>{item.attributes.Provinsi}</Text>
                            <MaterialCommunityIcons
                              name="chevron-right"
                              size={25}
                              style={{ color: "#3498DB", alignSelf:'stretch' }}                              
                            />
                        </TouchableOpacity>
                      </View>
                    )}
                    keyExtractor={({ item }, index) => index.toString()}
                  />
                                                  
            </View>
        )
    }

//end of HomeScreen
}

  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    headerText: {
      fontSize: 18,
      fontWeight: 'bold'
    },
    button: {
      alignItems: "center",
      backgroundColor: "#DDDDDD",
      padding: 10,
      marginBottom: 10,
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },
    wrapperItem: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems:'stretch',
      backgroundColor: '#dddddd',    
    },
    //? Lanjutkan styling di sini
    itemContainer: {
      width: DEVICE.width * 0.44,
      flexDirection: 'column',
      justifyContent:'center',
      alignItems: 'center',
      backgroundColor: '#ffffff',
      padding:5,
      marginVertical: 10    
    },
    itemImage: {
      width: 100,
      height: 100,
      justifyContent: 'center',
      alignSelf: 'center',
    },
    itemName: {
      fontWeight: 'bold',
      justifyContent: 'center',
      alignSelf: 'center',
      marginBottom: 20
    },
    itemPrice: {
    },
    itemStock: {
    },
    itemButton: {
      flex: 1
    },
    buttonText: {
    },
    item: {
      backgroundColor: '#f9c2ff',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 16,
    },
    title: {
      fontSize: 32,
    },    
  })