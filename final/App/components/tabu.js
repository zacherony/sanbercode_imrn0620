import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { Entypo } from '@expo/vector-icons';
// import { useNavigation } from '@react-navigation/native';

export default class Tabu extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          searchText: '',
          totalPrice: 0,
        }
      }    
    render() {
        // const navigation = useNavigation();
        console.log(this.props);
        
        return (    
            <View style={styles.tabu}>
                <TouchableOpacity 
                style={styles.button}
                onPress={() => this.props.navigation.navigate("Utama") }
                >
                    <Entypo
                        name="home"
                        size={20}
                        style={{ color: "#fff", alignItems:'center' }}
                    />
                    <Text>Home</Text>
                </TouchableOpacity>
                <TouchableOpacity 
                style={styles.button}
                onPress={() => this.props.navigation.navigate("About") }
                >
                    <Entypo
                        name="info-with-circle"
                        size={20}
                        style={{ color: "#fff", alignItems:'center' }}
                    />
                    <Text>About</Text>
                </TouchableOpacity>                    
            </View>
        )
    }
}

    const styles = StyleSheet.create({
        container: {
          flex: 1,
        },
        button: {
          alignItems: "center",
          backgroundColor: "#809fff",
          padding: 10,
          marginBottom: 10,
          justifyContent: 'flex-start',
          width: '50%'
          
        },
        tabu: {
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'stretch'
        }
    })