	/* 
	    Soal no. 1 ----------------------------
	*/

	function teriak() {
		return "Halo Sanbers"
	}

	console.log( teriak() )
	
	/* 
	    Soal no. 2 ----------------------------
	*/
	function multiply( angka1 = 5, angka2 = 6 ) {
		return angka1 * angka2
	}

	var num1 = 12
	var num2 = 4
	 
	var hasilKali = multiply(num1, num2)
	console.log( hasilKali ) // 30 

	/* 
	    Soal no. 3 ----------------------------
	*/

	var introduce = function(nama, umur, alamat, hobi) {
	  return "Nama saya "+nama+", umur saya "+umur+" tahun, alamat saya di "+alamat+", dan saya punya hobby yaitu "+hobi+"!"
	}	

	var name = "Zahrony D Martanto"
	var age = 30
	var address = "Jl. Patimura Raya Kabupaten Semarang"
	var hobby = "Gaming"
	 
	var perkenalan = introduce(name, age, address, hobby)
	console.log(perkenalan) // Menampilkan "Nama saya Zahrony D Martanto, umur saya 30 tahun, alamat saya di Jl. Patimura Raya No.09 Kabupaten Semarang, dan saya punya hobby yaitu Gaming!"