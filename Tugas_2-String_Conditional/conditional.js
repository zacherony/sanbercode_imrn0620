
// Tugas Conditional - If-else 
	var nama = "Parjo"
	var peran = "Guard"

	if( nama=="" ) {
		console.log('Nama harus diisi!')

	} else if( nama!=="" && peran=="" ) {
		console.log('Halo '+nama+', Pilih Peranmu untuk memulai game!')

	} else if( nama!=="" && peran!=="" ) {

		if (peran=="Penyihir") {
			cast = "Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!"

		} else if (peran=="Guard") {
			cast = "Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf."

		} else if (peran=="Werewolf") {
			cast = "Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!" 

		}

		console.log( "Selamat datang di Dunia Werewolf, " + nama )
		console.log( cast )
	}


// Tugas Conditional - Switch Case 
	var hari = 21; 
	var bulan = 1; 
	var tahun = 1945;
	//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 


	switch(bulan) {
	  case 1:   { bln = "Januari"; break; }
	  case 2:   { bln = "Februari"; break; }
	  case 3:   { bln = "Maret"; break; }
	  case 4:   { bln = "April"; break; }
	  case 5:   { bln = "Mei"; break; }
	  case 6:   { bln = "Juni"; break; }
	  case 7:   { bln = "Juli"; break; }
	  case 8:   { bln = "Agustus"; break; }
	  case 9:   { bln = "September"; break; }
	  case 10:  { bln = "Oktober"; break; }
	  case 11:  { bln = "November"; break; }
	  case 12:  { bln = "Desember"; break; }
	  default:  { console.log('Tidak terjadi apa-apa'); }
	}

	console.log( hari +' '+ bln +' '+ tahun )