// No. 1 Looping While
	var batas = 2
	console.log('LOOPING PERTAMA')
	while( batas < 21 ) {
		console.log(batas + ' - I love coding')
		batas += 2
	}

	var batas = 20
	console.log('LOOPING KEDUA')
	while( batas > 1 ) {
		console.log(batas + ' - I will become a mobile developer')
		batas -= 2
	}

// No. 2 Looping menggunakan for
	console.log('OUTPUT')
	for (var i = 1; i < 21; i++) {

		if( Math.abs( i % 2 ) == 1 ) {
			if ( i % 3 == 0 ) {
				word = 'I Love coding'

			} else {
				word = 'Santai'

			}

		} else if( i % 2 == 0 ) {
			word = 'Berkualitas'

		}

		console.log(i + ' - ' + word)
	}

// No. 3 Membuat Persegi Panjang

	for (var i = 0; i < 4; i++) {
		console.log('#'.repeat(8))
	}

// No. 4 Membuat Tangga

	for (var i = 0; i < 8; i++) {
		console.log('#'.repeat(i))
	}

// No. 5 Membuat Papan Catur

	for (var i = 0; i < 8; i++) {
		if( i % 2 == 0) {
			console.log(' #'.repeat(4))
		} else {
			console.log('# '.repeat(4))
		}
	}