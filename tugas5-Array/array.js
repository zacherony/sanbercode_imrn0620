// Soal No. 1 (Range) 
console.log('Soal No. 1 (Range)')
console.log('------------------------------------------------------')
var range = function(num1=0, num2=0) {
	var arr = []
	var value = 0

	// jika parameter tidak di isi, maka nilai default adalah 0
	if( num1 == 0 || num2 == 0 ) {
		value = -1

	} else {

		// jika nilai awal lebih kecil dari nilai akhir
		if( num1 < num2 ) {		
			for (var a = num1; a <= num2; a++) {
				arr.push(a) 
			}

		} else {
			for (var b = num1; b >= num2 ; b--) {
				arr.push(b)
			}

		}
		value = arr
	}

	return value
}

console.log( range(1,10) )
console.log( range(1) )
console.log( range(11,18) )
console.log( range(54,50) )
console.log( range() )
console.log("\n")
		

console.log('Soal No. 2 (Range with Step)')
console.log('------------------------------------------------------')
var rangeWithStep = function( num1=0, num2=0, step=1 ) {
	var arr = []
	// jika nilai awal lebih kecil dari nilai akhir
	if( num1 < num2 ) {		
		for (var a = num1; a <= num2; a += step) {
			arr.push(a) 
		}

	} else {
		for (var b = num1; b >= num2 ; b-=step) {
			arr.push(b)
		}

	}

	return arr
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log("\n")

console.log('Soal No. 3 (Sum of Range)')
console.log('------------------------------------------------------')
function sum(a,b,step=1) {
	var arr = rangeWithStep(a,b,step)
	var len = arr.length
	var jum = 0
	for (var i = 0; i < len; i++) {
		jum += arr[i]
	}

	return jum
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log("\n")

console.log('Soal No. 4 (Array Multidimensi)')
console.log('------------------------------------------------------')
function dataHandling(data) {
	var print = ""
	var label = ["Nomor ID : ","Nama Lengkap : ","TTL : ", "Hobi : "]

	for (var i = 0; i < data.length; i++) {
	
		panjang = data[i].length -1
		for (var x = 0; x < panjang ; x++) {
			if(x==3) {
				print += label[x] + data[i][x+1] + '\n\n'
			} else if (x==2) {
				print += label[x] + data[i][x] +' '+ data[i][x+1] + '\n'
			} else {
				print += label[x] + data[i][x] + '\n'
			}
		}
	
	}
	return print
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]
console.log( dataHandling(input) )


console.log('Soal No. 5 (Balik Kata)')
console.log('------------------------------------------------------')

var balikKata = function(kata) {
	var kebalik = ""
	for (var i = kata.length - 1; i >= 0 ; i--) {
		kebalik += kata[i]
	}
	return kebalik
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("\n")


console.log('Soal No. 6 (Metode Array)')
console.log('------------------------------------------------------')

var namaBln = function(angka) {
	switch(angka) {
	  case 1:   { bln = "Januari"; break; }
	  case 2:   { bln = "Februari"; break; }
	  case 3:   { bln = "Maret"; break; }
	  case 4:   { bln = "April"; break; }
	  case 5:   { bln = "Mei"; break; }
	  case 6:   { bln = "Juni"; break; }
	  case 7:   { bln = "Juli"; break; }
	  case 8:   { bln = "Agustus"; break; }
	  case 9:   { bln = "September"; break; }
	  case 10:  { bln = "Oktober"; break; }
	  case 11:  { bln = "November"; break; }
	  case 12:  { bln = "Desember"; break; }
	  default:  { }
	}

	return bln
}

var sortingArrayDescending = function(arr) {
	return arr.sort(function(a,b){return b-a})
}

var joinArrayToString = function(arr) {
	return arr.join("-")
}

var limiting = function(nama) {
	return nama.slice(0,15)
}

var dataHandling2 = function(data) {
	newdata = data
	tampilkan = ""

	newdata.splice(-1, 1) //hapus hobi membaca
	newdata.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung") // edit nama dan alamat
	newdata.splice(4, 2, "Pria", "SMA Internasional Metro") // tambahkan 2 item, sex dan sekolah

	bln 		 = newdata[3].split("/")

	bulanNumeric = bln.splice(0, 3, parseInt(bln[0]), parseInt(bln[1]), parseInt(bln[2]) )

	namaBulan 	 = namaBln( +bln[1] )
	console.log(data)
	console.log(namaBulan)

	sorting 	 = sortingArrayDescending(bulanNumeric)
	console.log(sorting)

	joining 	 = joinArrayToString( newdata[3].split("/") )
	console.log(joining)

	limits 		 = limiting( newdata[1] )
	console.log(limits)	

	// tampilkan 	 += data + "\n"
	// tampilkan 	 += namaBulan + "\n"
	// tampilkan 	 += sorting + "\n"
	// tampilkan 	 += joining + "\n"
	// tampilkan 	 += limits + "\n"
	return ""
}


//contoh output
var roman = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
console.log(dataHandling2(roman) )